var expect = require("chai").expect;
const fetch = require("node-fetch");

const serve_URL = "https://oca-server.glitch.me/";

describe("Oca end to end", function () {
    describe("Iniciar partida", function () {
        it("Empezar partida", function () {
            let currentState = {
                numTurno: 0,
                turno: 0,
                movimientos:0,
                rojo: false,
                amarillo: false,
                enJuego: false,
                coordRojo: 0,
                coordAmarillo: 0,
                alerta: "Empezando",
            };
            let expected = 1;
            return fetch( `https://oca-server.glitch.me/iniciar`,
            {
                method: 'POST',
                headers: {"Content-Type": "application/json"},
                body: JSON.stringify(currentState)
            }).then( response => response.json() )
                .then( (response) => {
                    expect(currentState.coordAmarillo).to.equal(expected);
                });
            
        });
    });
    describe("Lanzar dado", function () {
        it("Lanzar el dado jugador rojo", function () {
            let estado = {
                numTurno: 2,
                turno: 0,
                movimientos: 2,
                rojo: true,
                amarillo: false,
                enJuego: true,
                coordRojo: 8,
                coordAmarillo: 8,
                alerta: "Turno jugador rojo",
            };
            var expected = 8;
            return fetch( `https://oca-server.glitch.me/dado`,
            {
                method: 'POST',
                headers: {"Content-Type": "application/json"},
                body: JSON.stringify(estado)
            }).then( response => response.json() )
                .then( (response) => {
                    expect(estado.coordRojo).to.not.equal(expected);
                });
        });
    });

});