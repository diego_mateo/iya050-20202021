var assert    = require("chai").assert;
const { comprobarCasilla, iniciaPartida, lanzarDado } = require("../src/game.js");

describe("Oca unit test", function () {
    describe("Iniciar partida", function () {
        it("Empezar partida", function () {
            expect(iniciaPartida().coordRojo).to.eql(1);
        });
    });
    describe("Lanzamiento de dado", function () {
        it("Lanzar el dado jugador rojo", function () {
            let currentState = {
                numTurno: 2,
                turno: 0,
                movimientos: 3,
                rojo: true,
                amarillo: false,
                enJuego: true,
                coordRojo: 9,
                coordAmarillo: 8,
                alerta: "Turno jugador rojo",
            };
            var expected = 9;
            expect(lanzarDado(currentState).coordRojo).to.not.equal(expected);
        });
    });
    describe("Comprobar Casilla", function () {
        it("De oca 5 a oca 9", function () {
            let currentState = {
                numTurno: 1,
                turno: 0,
                movimientos: 1,
                rojo: true,
                amarillo: false,
                enJuego: true,
                coordRojo: 5,
                coordAmarillo: 4,
                alerta: "Avanza 5",
            };
            var expected = 9;
            expect(comprobarCasilla(currentState).coordRojo).to.equal(expected);
        });
        it("De oca 9 a oca 14", function () {
            let currentState = {
                numTurno: 2,
                turno: 0,
                movimientos: 3,
                rojo: false,
                amarillo: true,
                enJuego: true,
                coordRojo: 9,
                coordAmarillo: 9,
                alerta: "Avanza 5",
            };
            var expected = 14;
            expect(comprobarCasilla(currentState).coordAmarillo).to.equal(expected);
        });
        it("De oca 59 a meta", function () {
            let currentState = {
                numTurno: 2,
                turno: 0,
                movimientos: 3,
                rojo: false,
                amarillo: true,
                enJuego: true,
                coordRojo: 56,
                coordAmarillo: 59,
                alerta: "Avanza 5",
            };
            var expected = 63;
            expect(comprobarCasilla(currentState).coordAmarillo).to.equal(expected);
        });
        it("Retrocede casillas", function () {
            let currentState = {
                numTurno: 2,
                turno: 0,
                movimientos: 3,
                rojo: false,
                amarillo: true,
                enJuego: true,
                coordRojo: 56,
                coordAmarillo: 65,
                alerta: "Avanza 5",
            };
            var expected = 61;
            expect(comprobarCasilla(currentState).coordAmarillo).to.equal(expected);
        });
        it("Cambio de turno", function () {
            let currentState = {
                numTurno: 2,
                turno: 0,
                movimientos: 3,
                rojo: true,
                amarillo: false,
                enJuego: true,
                coordRojo: 9,
                coordAmarillo: 8,
                alerta: "Avanza 5",
            };
            var expected = {
                numTurno: 3,
                turno: 0,
                movimientos: 4,
                rojo: false,
                amarillo: true,
                enJuego: true,
                coordRojo: 14,
                coordAmarillo: 8,
                alerta: "De oca a oca y tiro porque me toca",
            };
            expect(comprobarCasilla(currentState)).to.equal(expected);
        });
    });
});