const lanzarDado = () => {
    return Math.round(Math.random()*(6-1)+1);
    
}

const mover = (ficha) => {
    let avance = lanzarDado();
    alerta("Avanza "+ (avance) + " casillas");
}

module.exports = { lanzarDado };