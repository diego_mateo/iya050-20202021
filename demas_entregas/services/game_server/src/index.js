const express = require("express");

const cors = require("cors");
const { iniciaPartida, lanzarDado } = require("./game.js");

const app = express();
const port = process.env.PORT;

//En esta version de express ya no hace falta body parse https://stackoverflow.com/questions/24330014/bodyparser-is-deprecated-express-4
app.use(express.json());
app.use(express.urlencoded({
  extended: true
}));

app.use(cors());

app.post("/iniciar", (req, res) => {
  var games = req.body;
  games.state = iniciaPartida(games);
  res.send(games.state);
});

app.post("/dado", (req, res) => {
  var games = req.body;
  games.state = lanzarDado(games);
  res.send(games.state);
});

app.listen(port, () => {
  console.log(`Server listening at http://localhost:${port}`);
 });
 
