let casillasRoja = [[],[125,590],[240,590],[300,590],[350,590],[405,590],[460,590],[518,590],[565,590],[595,560],[595,510],[595,460],[595,408],[595,353],[595,300],[595,248],[595,195],[595,142],[595,95],[565,65],[515,65],[465,65],[410,65],[360,65],[306,65],[253,65],[200,65],[147,65],[101,65],[67,94],[69,147],[69,200],[69,253],[69,308],[69,365],[69,418],[69,468],[100,495],[150,495],[203,495],[255,495],[308,495],[363,495],[417,495],[467,495],[503,465],[503,410],[503,352],[503,300],[503,245],[503,190],[470,155],[417,155],[361,155],[305,155],[250,155],[197,155],[165,197],[165,252],[165,310],[165,365],[195,407],[250,407],[310,330],[],[],[],[],[]];
let casillasAmarilla = [[],[125,625],[240,625],[300,625],[350,625],[405,625],[460,625],[518,625],[575,625],[630,580],[630,510],[630,460],[630,408],[630,353],[630,300],[630,248],[630,195],[630,142],[630,70],[580,30],[515,30],[465,30],[410,30],[360,30],[306,30],[253,30],[200,30],[147,30],[80,30],[35,75],[35,147],[35,200],[35,253],[35,308],[35,365],[35,418],[35,488],[80,530],[150,530],[203,530],[255,530],[308,530],[363,530],[417,530],[480,530],[540,485],[540,410],[540,352],[540,300],[540,245],[540,160],[500,120],[417,120],[361,120],[305,120],[250,120],[170,120],[128,170],[128,252],[128,310],[128,385],[170,442],[250,442],[350,330],[],[],[],[],[]];
var alertaJuego;

const iniciaPartida = (gamestate) => {
	var turno = Math.round(Math.random()*(1));
	let state = sortearTurno(gamestate, turno);
	return state;
}

const lanzarDado = (state, moves) => {
	let avance = Math.round(Math.random()*(6-1)+1);
	state = avanzarCasillas(state, avance, moves);
	return state;
}

const avanzarCasillas = (gamestate, avance) => {
	
	alerta("Avanza "+ (avance) + " casillas");
	if(gamestate.rojo === true){
		gamestate.coordRojo += avance;
	}
	else if(gamestate.amarillo === true){
		gamestate.coordAmarillo += avance;
	}
	gamestate.movimientos += 1;
	gamestate.listMoves.push(avance);
	gamestate.alerta = alertaJuego;
	state = comprobarCasilla(gamestate);
	return state;
}

const sortearTurno = (state, turno) => {
	state.numTurno = 1;
	state.turno = turno;
	state.movimientos = 0;
	state.enJuego = true;
	state.coordRojo = 1;
	state.coordAmarillo = 1;
	state.listMoves = [];
	state.alerta = "Tira el dado";
	if(turno === 0){
		state.rojo = true;
		state.amarillo = false;
	}
	if(turno == 1){
		state.amarillo = true;
		state.rojo = false;
	}
	return state;
}

function cambiarTurno(state){
	if(state.rojo === true){
		state.rojo = false;
		state.amarillo = true;
		state.coordRojo = casillaEspecial(state.coordRojo);
	}
	else if(state.amarillo === true){
		state.amarillo = false;
		state.rojo = true;
		state.coordAmarillo = casillaEspecial(state.coordAmarillo);
	}
	state.alerta = alertaJuego;
	return state;
}

const comprobarCasilla = (gamestate) => {
	
	gamestate = cambiarTurno(gamestate);
	gamestate = comprobarEstado(gamestate);
	if (gamestate.turno === 0 && gamestate.amarillo === true){
		gamestate.numTurno += 1;
	}else if(gamestate.turno === 1 && gamestate.amarillo === false){
		gamestate.numTurno += 1;
	}

	return gamestate;
}

const comprobarEstado = (state) => {
	
	if (state.coordRojo === 63 || state.coordAmarillo === 63){
		state.rojo = false;
		state.amarillo = false;
		state.enJuego = false;
		state.alerta = "Fin del juego";
	}
	return state;
}

const casillaEspecial = (casilla) => {
	var avanza = casilla;
    if(casilla === 5){
		avanza = oca_4(casilla);
	}
	else if(casilla === 9){
		avanza = oca_5(casilla);
	}
	else if(casilla === 14){
		avanza = oca_4(casilla);
	}
	else if(casilla === 18){
		avanza = oca_5(casilla);
	}
	else if(casilla === 23){
		avanza = oca_4(casilla);
	}
	else if(casilla === 27){
		avanza = oca_5(casilla);
	}
	else if(casilla === 32){
		avanza = oca_4(casilla);
	}
	else if(casilla === 36){
		avanza = oca_5(casilla);
	}
	else if(casilla === 41){
		avanza = oca_4(casilla);
	}
	else if(casilla === 45){
		avanza = oca_5(casilla);
	}
	else if(casilla === 50){
		avanza = oca_4(casilla);
	}
	else if(casilla === 54){
		avanza = oca_5(casilla);
	}
	else if(casilla === 59){
		avanza = oca_59(casilla);
	}
	//Puentes
	else if(casilla === 6){
		avanza = puente_6(casilla);
	}
	else if(casilla === 12){
		avanza = puente_12(casilla);
	}
	//Dados
	else if(casilla === 26){
		avanza = dado_26(casilla);
	}
	else if(casilla === 53){
		avanza = dado_53(casilla);
	}
	//Calavera
	else if(casilla === 58){
		avanza = calavera(casilla);
	}
	//Laberinto
	else if(casilla === 42){
		avanza = laberinto(casilla);
	}
	//Casillas mas alla de la meta
	else if(casilla === 64){
		avanza = retrocede_1(casilla);
	}
	else if(casilla === 65){
		avanza = retrocede_2(casilla);
	}
	else if(casilla === 66){
		avanza = retrocede_3(casilla);
	}
	else if(casilla === 67){
		avanza = oca_59(casilla);
	}
	else if(casilla === 68){
		avanza = calavera(casilla);
	}
	return avanza;
}

//Funcion al caer en la casilla de oca
const oca_4 = (casilla) => {
	alerta('"De oca a oca y tiro porque me toca"');
	casilla += 4;
	return casilla;
}
const oca_5 = (casilla) =>{
	alerta('"De oca a oca y tiro porque me toca"');
	casilla += 5;
	return casilla;
}
const oca_59 = (casilla) => {
	alerta('"Gane"');
	casilla += 4;
	return casilla;
}
//Funcion para los puentes
const puente_6 = (casilla) => {
	alerta('"De puente a puente y tiro porque me lleva la corriente"');
	casilla += 6;
	return casilla;
}
const puente_12 = (casilla) => {
	alerta('"De puente a puente y tiro porque me lleva la corriente"');
	casilla -= 6;
	return casilla;
}
//Funcion para los dados
const dado_26 = (casilla) => {
	alerta('"De dados a dados y tiro porque me ha tocado"');
	casilla += 27;
	return casilla;
}
const dado_53 = (casilla) => {
	alerta('"De dados a dados y tiro porque me ha tocado"');
	casilla -= 27;
	return casilla;
}
//Funcion calavera
const calavera = (casilla) => {
	alerta('"Vuelves a empezar"');
	casilla -= 57;
	return casilla;
}
//Funcion para el laberinto
const laberinto = (casilla) => {
	alerta('"Has caido en el laberinto"');
	casilla -= 12;
	return casilla;
}
//Funciones por si te pasas de largo
const retrocede_1 = (casilla) => {
	alerta('"Retrocede una casilla"');
	casilla -= 2;
	return casilla;
}
const retrocede_2 = (casilla) =>{
	alerta('"Retrocede 2 casillas"');
	casilla -= 4;
	return casilla;
}
const retrocede_3 = (casilla) => {
	alerta('"Retrocede 3 casillas"');
	casilla -= 6;
	return casilla;
}

function alerta(mensaje){
	alertaJuego = mensaje;
}

module.exports.comprobarCasilla = comprobarCasilla;
module.exports.iniciaPartida = iniciaPartida;
module.exports.lanzarDado = lanzarDado;