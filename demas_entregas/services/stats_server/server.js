const {GraphQLServer} = require("graphql-yoga");
const fetch = require("node-fetch");
const {v4: uuidv4} = require('uuid');

const typeDefs = `
    type Query {
        player(name: String!): Player,
        partida(id: ID!, name: String!): Partida
    }
    type Mutation {
        createPlayer(name: String!): String,
        createPartida(name: String!,nTurns: Int!,turno: Int!,nMoves: Int!, rojo: Boolean!,amarillo: Boolean!, coordRojo: Int!, coordAmarillo: Int!,alerta: String!): String,
        updatePartida(id: String!, name: String!, nTurns: Int!, turno: Int!, nMoves: Int!, rojo: Boolean!, amarillo: Boolean!, coordRojo: Int!, coordAmarillo: Int!, alerta: String!, listMoves: [Int]!, complete: Boolean!): String
    }
    type Player {
        id: ID,
        name: String,
        partidas: [Match]
    }
    type Partida {
        id: ID,
        name: String,
        nTurns: Int,
        turno: Int,
        nMoves: Int,
        rojo: Boolean,
        amarillo: Boolean,
        coordRojo: Int,
        coordAmarillo: Int,
        alerta: String,
        listMoves: [Int],
        complete: Boolean
    }
    type Match {
        id: ID,
        name: String,
        nTurns: Int,
        turno: Int,
        nMoves: Int,
        rojo: Boolean,
        amarillo: Boolean,
        coordRojo: Int,
        coordAmarillo: Int,
        alerta: String,
        listMoves: [Int],
        complete: Boolean
    }
`

const BASE_URL = "https://0qh9zi3q9g.execute-api.eu-west-1.amazonaws.com/development";
const config = {method: "GET", headers: {"x-application-id": "diego-mateo-test"}}

const fetchPut = (name, body) => {

    fetch(`${BASE_URL}/pairs/${name}`,
        {
            method: 'PUT',
            headers: {
                "x-application-id": "diego-mateo-test",
                "Content-Type": "application/json",
            },
            body: JSON.stringify(body)
        })
        .then(response => response.json())
        .catch( error => "putData : error");

}

const fetchGet = (name) => {
    return fetch(`${BASE_URL}/pairs/${name}`,
        {
            method: 'GET',
            headers: {"x-application-id": "diego-mateo-test"}
        })
        .then(response => response)
        .then(response => response.json())
        .catch( error => "getData : error");
}

const resolvers = {
    Query: {
        player: async (_, { name }) => 
            fetchGet("user-" + name)
                .then(res => res),
        partida: (_, { id, name }) => 
            fetchGet("user-" + name + "-p-" + id)
            .then(res => res),        
    },
    Mutation: {
        createPlayer: (_, { name }) => fetchGet("user-"+name).then(player => {
            if (typeof player.value === 'undefined') {
                console.log(name);
                let id = uuidv4();
                var json = {
                  "id" : id,
                  "name": name,
                  "partidas": []
                };
                fetchPut("user-"+name, json);
                return id;
            }else{
                console.log(JSON.parse(player.value).id);
                return "nop";
            }
        }),
        createPartida: (_, { name, nTurns,turno,nMoves, rojo,amarillo, coordRojo, coordAmarillo,alerta }) => fetchGet("user-"+name).then(player => {
            //  createPartida(name: "prueba2", nTurns: 0, turno: 0, nMoves: 0, rojo: false, amarillo: false, coordRojo:0,coordAmarillo:0,alerta:"hola")
            console.log(name);
            if (typeof player.value !== 'undefined') {
                
                let idplayer = JSON.parse(player.value).id;
                console.log(idplayer);
                let partidas = JSON.parse(player.value).partidas;
                let id = uuidv4();
                        
                let partida = 
                {
                "id": id,
                "name": name,
                "nTurns": nTurns,
                "turno": turno,
                "nMoves": nMoves,
                "rojo": rojo,
                "amarillo": amarillo,
                "coordRojo": coordRojo,
                "coordAmarillo": coordAmarillo,
                "alerta": alerta,
                "listMoves": [],
                "complete": false
                };
                fetchPut("user-" + name + "-p-" + id, partida);
                console.log(partida);
                console.log("----------------");
                partidas.push(partida);
                console.log(partidas);
                var json = {
                "id" : idplayer,
                "name": name,
                "partidas": partidas
                };
                fetchPut("user-"+name, json);
                return id;
            }else{console.log("error");}
        }),
        updatePartida: (_, { id, name, nTurns, turno, nMoves, rojo, amarillo, coordRojo, coordAmarillo, alerta, listMoves, complete }) => fetchGet("user-"+name).then(player => {
            if (typeof player.value !== 'undefined') {
                let idplayer = JSON.parse(player.value).id;
                console.log(idplayer);
                let partidas = JSON.parse(player.value).partidas;
                        
                let partida = 
                {
                "id": id,
                "name": name,
                "nTurns": nTurns,
                "turno": turno,
                "nMoves": nMoves,
                "rojo": rojo,
                "amarillo": amarillo,
                "coordRojo": coordRojo,
                "coordAmarillo": coordAmarillo,
                "alerta": alerta,
                "listMoves": listMoves,
                "complete": complete
                }
                ;
                fetchPut("user-" + name + "-p-" + id, partida);
                partidas = partidas.map(part => {
                    if (part.id === id){
                        part = partida;
                        console.log("hola");
                        console.log(part);
                        console.log("----------------");                       
                    }
                    return part;
                })          
                console.log(partidas);
                var json = {
                "id" : idplayer,
                "name": name,
                "partidas": partidas
                };
                fetchPut("user-"+name, json);
                return id;
            }else{return "error";}
        }),
    },
    Player: {
        id: (data) => JSON.parse(data.value).id,
        name: (data) => JSON.parse(data.value).name,
        partidas: (data) => {
            return JSON.parse(data.value).partidas.map(partida => ({
                id: partida.id,
                name: partida.name,
                nTurns: partida.nTurns,
                turno: partida.turno,
                nMoves: partida.nMoves,
                rojo: partida.rojo,
                amarillo: partida.amarillo,
                coordRojo: partida.coordRojo,
                coordAmarillo: partida.coordAmarillo,
                alerta: partida.alerta,
                listMoves: partida.listMoves,
                complete: partida.complete,
            }))
        },
    },
    Partida: {
        id: (data) => JSON.parse(data.value).id,
        name: (data) => JSON.parse(data.value).name,
        nTurns: (data) => JSON.parse(data.value).nTurns,
        turno: (data) => JSON.parse(data.value).turno,
        nMoves: (data) => JSON.parse(data.value).nMoves,
        rojo: (data) => JSON.parse(data.value).rojo,
        amarillo: (data) => JSON.parse(data.value).amarillo,
        coordRojo: (data) => JSON.parse(data.value).coordRojo,
        coordAmarillo: (data) => JSON.parse(data.value).coordAmarillo,
        alerta: (data) => JSON.parse(data.value).alerta,
        listMoves: (data) => JSON.parse(data.value).listMoves,
        complete: (data) => JSON.parse(data.value).complete,
    },
    Match: {
        id: (data) => data.id,
        name: (data) => data.name,
        nTurns: (data) => data.nTurns,
        turno: (data) => data.turno,
        nMoves: (data) => data.nMoves,
        rojo: (data) => data.rojo,
        amarillo: (data) => data.amarillo,
        coordRojo: (data) => data.coordRojo,
        coordAmarillo: (data) => data.coordAmarillo,
        alerta: (data) => data.alerta,
        listMoves: (data) => data.listMoves,
        complete: (data) => data.complete,
    },
}

const server = new GraphQLServer({typeDefs, resolvers});

server.express.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header(
      "Access-Control-Allow-Headers",
      "Origin, X-Requested-With, Content-Type, Accept, privatekey"
    );
    next();
  });
  
  const opts = {
    cors: {
      credentials: true,
      origin: ["*"],
    },
    port: process.env.PORT,
    playground: "/",
  };
  
  server.start(opts).then(console.log("Servidor Stats GraphQL"));