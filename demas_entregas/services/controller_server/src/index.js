const Koa = require("koa");
const cors = require("@koa/cors");
const koaBody = require("koa-body");
const Router = require("@koa/router");
const fetch = require("node-fetch");

const app = new Koa();
const router = new Router();

const query_URL = "http://game_server:" + process.env.GRAPH;
const serve_URL = "http://game_server:" + process.env.GAMESERVE;

const query_post = (query) =>
 fetch(query_URL, {
   method: "POST",
   headers: { "Content-Type": "application/json" },
   body: JSON.stringify({
     query: query,
   }),
 }).then((res) => res.json());

 const serve_post = (action, game) => {
  fetch(serve_URL + action, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: game,
  }).then((response) => {
  if (response.ok) {
      return response.json();
  }
  throw "Fetch put error";
  });     
}

router.post("/game", async(ctx) => {
  // 1. post mutation to Stats server to store new game
  let game = {
    numTurno: 0,
    turno: 0,
    movimientos: 0,
    rojo: false,
    amarillo: false,
    enJuego: false,
    coordRojo: 0,
    coordAmarillo: 0,
    alerta: "Sorteando el turno",
  };
  let id;
  // 2. return state
  await query_post(
    `mutation{createPartida(name:"${ctx.request.body.name}",nTurns:${game["numTurno"]},turno:${game["turno"]},nMoves:${game["movimientos"]},rojo:${game["rojo"]},amarillo:${game["amarillo"]},coordRojo:${game["coordRojo"]},coordAmarillo:${game["coordAmarillo"]},alerta:${game["alerta"]})}`
  ).then((e) => (id = e.data.createPartida));

  ctx.response.set("Content-Type", "application/json");
  ctx.body = JSON.stringify({
    gamestate: game,
    id,
  });
});

router.get("/game/:id", async(ctx) => {
  // 1. get current state from Stats server
  let game = {};

  await query_post(
    `query{partida(id:"${ctx.params.id}",name:"${ctx.request.body.name}"){nTurns,turn,nMoves,rojo,amarillo,coordRojo,coordAmarillo,alerta,listMoves,complete}}`
  )
    .then((e) => e.data.partida)
    .then((e) => {
      listMoves = e.listMoves;
      game["numTurno"] = e.nTurns;
      game["turno"] = e.turno;
      game["movimientos"] = e.nMoves;
      game["rojo"] = e.rojo;
      game["amarillo"] = e.amarillo;
      game["enJuego"] = e.complete;
      game["coordRojo"] = e.coordRojo;
      game["coordAmarillo"] = e.coordAmarillo;
      game["alerta"] = e.alerta;
      game["listMoves"] = e.listMoves;
    });
  // 2. return state

  ctx.response.set("Content-Type", "application/json");
  ctx.body = JSON.stringify({
    id: ctx.params.id,
    game,
  });
});

router.post("/game/:id/event", async(ctx) => {
  // 1. get current state from Stats server
  let game;
  let event = ctx.request.body.data.event;

  await query_post(
    `query{partida(id:"${ctx.params.id}",name:"${ctx.request.header.name}"){nTurns,turn,nMoves,rojo,amarillo,coordRojo,coordAmarillo,alerta,listMoves,complete}}`
  )
    .then((e) => e.data.partida)
    .then((e) => {
      listMoves = e.listMoves;
      game["numTurno"] = e.nTurns;
      game["turno"] = e.turno;
      game["movimientos"] = e.nMoves;
      game["rojo"] = e.rojo;
      game["amarillo"] = e.amarillo;
      game["enJuego"] = e.complete;
      game["coordRojo"] = e.coordRojo;
      game["coordAmarillo"] = e.coordAmarillo;
      game["alerta"] = e.alerta;
      game["listMoves"] = e.listMoves;
    });

  let body = JSON.stringify(game);
  // 2. get next state from Game server
  let newstate;
  await serve_post(`/${event}`, body).then((e) => {
    newstate = e;
  });

  // 3. post mutation to Stats server to store next state
  await query_post(
    `mutation{updatePartida(id:"${ctx.params.id}",name:"${ctx.request.header.name}",numTurno:${newstate.numTurno},turno:${newstate.turno},nMoves:${newstate.movimientos},rojo:${newstate.rojo},amarillo:${newstate.amarillo},coordRojo:${newstate.coordRojo},coordAmarillo:${newstate.coordAmarillo},alerta:${newstate.alerta},listMoves:[${newstate.listMoves}],complete:${newstate.enJuego})}`
  );
  // 4. return state

  ctx.response.set("Content-Type", "application/json");
  ctx.body = JSON.stringify({
    id: ctx.params.id,
    gamestate: newstate,
  });
});

app.use(koaBody());
app.use(cors());
app.use(router.routes());
app.use(router.allowedMethods());

app.listen(process.env.PORT);
