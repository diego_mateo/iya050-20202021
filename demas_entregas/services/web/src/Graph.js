const graphQuery = (query) =>
   fetch("https://https://graph-server-dm.glitch.me/", {
       method: "POST",
       headers: { "Content-Type": "application/json" },
       body: JSON.stringify({
           query: query,
       }),
   }).then((res) => res.json())
 
export default graphQuery;