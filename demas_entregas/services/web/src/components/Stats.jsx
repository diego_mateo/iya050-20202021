import React, { useContext } from "react";
import AuthContext from "../AuthContext.js";

const Stats = () => {
  const { user } = useContext(AuthContext);
  const query_URL = "https://graph-server-dm.glitch.me";
  const query_post = (name) =>
    fetch(query_URL, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        query: `
              query {
                player(name:"${name}"){
                  partidas{
                    id
                    nTurns
                    turno
                    nMoves               
                    complete
                  }
                }
              }
            `,
      }),
    }).then((res) => res.json());

  const playerData = () => {
      query_post(user.name)
      .then((res) =>{
        console.log (res.data.player.partidas);
        pintar(res.data.player.partidas);
      })
      .catch((error) => console.log(error, "error al cargar los datos"));
  };

  const pintar = (partidas) => {
    const arr = Object.entries(partidas);
    const tabla = document.getElementById("dataStats");
    var texto = "";
    var color;
    var enjuego;
    for(let i =0; i < arr.length; i++){
        if(arr[i][1].turno === 0){
          color = "rojo";
        }else{
          color = "amarillo";
        }
        if(arr[i][1].complete === true){
          enjuego = "Terminada";
        }else{
          enjuego = "En juego";
        }
        texto = texto+`<tr><td>${arr[i][1].id}</td><td>${color}</td><td>${enjuego}</td><td>${arr[i][1].nTurns}</td><td>${arr[i][1].nMoves}</td></tr>`;
    }
    tabla.innerHTML=texto;
  }

  const data = playerData();
  console.log(data);
  return (
    <div>
        <p>{user.name}’s stats</p>
        <table>
            <thead>
              <tr>
                <th>Partida</th>
                <th>Ficha</th>
                <th>Estado del juego</th>
                <th>Numero de turnos</th>
                <th>Numero de movimientos</th>
              </tr>
            </thead>
            <tbody id="dataStats">

            </tbody>
        </table>
    </div>);


};

export default Stats;
