import React, { useContext } from "react";
import AuthContext from "../AuthContext.js";

const Play = () => {
  const { user } = useContext(AuthContext);
  var key;
  var datos = [];
  const nuevaPartida = () => {

    var game = JSON.stringify({
      "name": user.name
    });

    let config = {
      method: 'POST',
      headers: {'Content-Type': 'application/json'},
      body: game,
    };

    fetch("http://localhost:3003/game", config)
      .then(response => response.json())
      .then(r => {
          console.log(r, r.id)
          key = r.id;
          return r;
      })
      .then(result => console.log(result))
      
    sortearTurno();
  } 

  const dibujar = () => {

    var game = JSON.stringify({
      "name": user.name
    });

    let config = {
      method: 'POST',
      headers: {'Content-Type': 'application/json'},
      body: game,
    };

    fetch("http://localhost:3003/game/"+key, config)
      .then(response => response.json())
      .then(r => {
          console.log(r);
          datos = r.game;
          return r;
      })
      .then(result => console.log(result));
      
  }

  const sortearTurno = () => {

    var game = JSON.stringify({
        "event": "iniciar"
    });

    var config = {
        method: 'POST',
        headers: {"Content-Type" : "application/json"},
        body: game,
    };

    fetch("http://localhost:3003/game/"+key+"/event", config)
      .then(response => response.text())
      .then(result => console.log(result))
      .catch(error => console.log('error', error));
  
    dibujar();
  }

  const tirarDado = () => {

    var game = JSON.stringify({
        "event": "dado"
    });

    var config = {
        method: 'POST',
        headers: {"Content-Type" : "application/json"},
        body: game,
    };

    fetch("http://localhost:3003/game/"+key+"/event", config)
      .then(response => response.text())
      .then(result => console.log(result))
      .catch(error => console.log('error', error));

    dibujar();
  }

  return <div>
    <div className="marcador">
      <h3>Bienvenido a la Oca:</h3><br/>
      <h3>{user.name}</h3>
    </div>
    <div className="normas">
      <p>Se juega con 2 jugadores. Al comienzo se sortea turno. Las fichas avanzan conforme  al dado. Dependiendo de la casilla en la que se caiga se puede avanzar o por el contrario retroceder. Gana el juego el primer jugador que llega a la meta.</p>
      <ol>
        <li>Oca: se avanza hasta la siguiente oca y se vuelve a tirar.</li>
        <li>El Puente: ubicadas en la casilla 6 y 12, se avanza o retrocede hasta el otro puente.</li>
        <li>Los Dados: se avanza o se retrocede a la otra casilla. Casillas 26 y 52.</li>
        <li>El Laberinto: retroceder  a la casilla 30. Casilla 42.</li>
        <li>La Muerte: se  vuelve a empezar desde la casilla 1. Casilla 58</li>
      </ol>
      <button onClick={nuevaPartida}>Nueva partida</button>
    </div>
    <div className="content-centered game-btn">
      <button onClick={tirarDado}>Tirar Dado</button>
    </div>
    <div>
    <p>Game</p>
        <table>
            <thead>
              <tr>
                <th>Ficha</th>
                <th>Turno</th>
                <th>Numero de turnos</th>
                <th>Movimientos</th>
                <th>Posicion jugador rojo</th>
                <th>Posicion jugador amarillo</th>
                <th>Mensaje</th>
              </tr>
            </thead>
            <tbody>
              {
                datos?.map((g)=>{
                  let color;
                  let turn;
                  if(g.turno===0){
                    color = "rojo";
                  }else{
                    color = "amarillo";
                  }
                  if(g.rojo===true){
                    turn = "rojo";
                  }else{
                    turn = "amarillo";         
                  }
                  return (
                    // eslint-disable-next-line react/jsx-key
                    <tr>                      
                      <td>{color}</td>
                      <td>{turn}</td>
                      <td>{g.numTurnos}</td>
                      <td>{g.movimientos}</td>
                      <td>{g.coordRojo}</td>
                      <td>{g.coordAmarillo}</td>
                      <td>{g.alerta}</td>
                    </tr>
                  )
              })
              }
            </tbody>
        </table>
    </div>
  </div>;
};

export default Play;
