import React from "react";
import { Link } from "react-router-dom";
import "./forms.css"

const Landing = () => {
  return (
    <div className="contact-wrap top-wrapper">
      <h1>Juego de la oca</h1>
      <p>
        Try <Link to="/sign-in">logging in</Link>.
      </p>
    </div>
  );
};

export default Landing;
