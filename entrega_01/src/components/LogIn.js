import React, {Component} from 'react';
import { Link, Redirect } from "react-router-dom";
import './Form.css';

class LogIn extends Component{

    constructor(props){
        super(props);
        this.state = {
            username: "",
            password: "",
            isLog: false,
            error: false,
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(e) {
        const {name, value} = e.target;
        this.setState({[name]: value});
    }

    handleSubmit(e){
        e.preventDefault();

        const user = this.state.username;
        const pass = this.state.password;
        const isLog = this.state.isLog;

        console.log(user);;
        console.log(pass);
        console.log(isLog);
        // stop here if form is invalid
        if (!(user && pass)) {
            return;
        }
        const url = "https://6fra5t373m.execute-api.eu-west-1.amazonaws.com/development/users/" + user + "/sessions";

        return fetch(url, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({ password: pass }),
        })
        .then((response) => response.json())
        .then((data) => {
            if (data.hasOwnProperty("sessionToken")) {
            localStorage.setItem("sessionToken", data.sessionToken);
            console.log(data.sessionToken);
            localStorage.setItem('username',  user);
            this.setState({ isLog: true });
            } else {
                this.setState({ error: true });
            }
        });

    }

    render(){

        const username = this.state.username;
        const password = this.state.password;
        const isReg = this.state.isReg;
        //const error = this.state.error;

        var redirect = "";
        if (this.state.isLog) {
            redirect = <Redirect to="/game" />;
        }

        return (
            <div className="contact-wrapper top-wrapper">
                <form className="form" onSubmit={this.handleSubmit}>
                    <h3>Acceder</h3>

                    <div className="input">
                        <label>Nombre de usuario</label>
                        <input type="text" name="username" value={username} onChange={this.handleChange} />
                        {isReg && !username && <div className="help-block">Nombre de usuario requerido</div>}
                    </div>

                    <div className="input">
                        <label>Contraseña</label>
                        <input type="text" name="password" value={password} onChange={this.handleChange} />
                        {isReg && !password && <div className="help-block">Contraseña requerida</div>}
                    </div>

                    <button type="submit" className="btn btn-primary btn-block">Sign Up</button>
                    <p className="forgot-password text-right">
                        ¿Todavia no estas registrado? <Link to="/registro">Haz click aqui</Link>
                    </p>
                </form>
                {redirect}
            </div> 
        );
    }

}

export default LogIn;