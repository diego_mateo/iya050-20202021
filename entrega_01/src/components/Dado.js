import React, {Component} from 'react';
import Dado_0 from './imagenes/dados_0.png';
import Dado_1 from './imagenes/dados_1.png';
import Dado_2 from './imagenes/dados_2.png';
import Dado_3 from './imagenes/dados_3.png';
import Dado_4 from './imagenes/dados_4.png';
import Dado_5 from './imagenes/dados_5.png';
import Dado_6 from './imagenes/dados_6.png';
import LogOut from './LogOut';
import './game.css';

class Dado extends Component{

    constructor(props){
        super(props);
        this.state = {
            resultado: 0,
            fondo : Dado_0,
            lista_resultados: [],
        }

        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(e) {
        const numero = Math.round(Math.random()*(6-1)+1);
        const lista_numeros = this.state.lista_resultados;
        lista_numeros.push(numero);
        var fondoD = Dado_0;
        switch (numero) {
            case 0:
                fondoD = Dado_0;
                break;
            case 1:
                fondoD = Dado_1;
                break;
            case 2:
                fondoD = Dado_2;
                break;
            case 3:
                fondoD = Dado_3;
                break;
            case 4:
                fondoD = Dado_4;
                break;
            case 5:
                fondoD = Dado_5;
                break;
            case 6:
                fondoD = Dado_6;
                break;
            default:
                fondoD = Dado_0;
        }
        const balck = fondoD;
        this.setState({resultado: numero});
        this.setState({fondo: balck});
        this.setState({lista_resultados: lista_numeros});
        console.log(numero);
        console.log(lista_numeros);
    }

    render(){

        const resultado = this.state.resultado;
        const lista_resultados = this.state.lista_resultados;
        const Background = {
            width: "150px",
            height: "150px",
            margin: "0 auto",
            backgroundImage:`url(`+this.state.fondo+`)`
        };
        return (
            <div>
                <LogOut />
                <div className="contact-wrap top-wrapper">
                    <label>Haz click sobre el dado para lanzar</label>
                    <button style={Background} onClick={this.handleChange}></button>
                    {resultado!==0 && <label>{"Ha salido " +resultado}</label>}
                </div>
                <div className="lista top-wrapper">
                    <h3>Lista de tiradas</h3>
                    {lista_resultados!==undefined && <ol>
                        {
                            lista_resultados.map((numero, i) => {
                                return(
                                    <li key={i}>
                                        {numero}
                                    </li>
                                );
                            })
                        }
                    </ol>}                    
                </div>               
            </div>
        );
    }

}

export default Dado;