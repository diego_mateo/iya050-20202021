import React from 'react';
import { Link } from 'react-router-dom';
import './game.css';

class LogOut extends React.Component{
    constructor (props) {
        super(props)
        this.state = {
          name: '',
        }
        this.getUser();
    }

    getUser(){
        const token = localStorage.getItem("sessionToken");
        console.log(token);
        const url = 'https://6fra5t373m.execute-api.eu-west-1.amazonaws.com/development/user';
        return fetch(url,{
            method: "GET",
            headers: {
                Authorization: token,
            },
        })
        .then((response) => response.json())
        .then((data) => {
            if (data.hasOwnProperty("firstName")){
                console.log("hola");
                this.setState({ name: data.firstName})
                console.log(data.firstName);
            }else{
                console.log(data);
                console.log("adios");
            }
        });
    }

    handleSubmit(e){
        const token = localStorage.getItem("sessionToken");
        const url = 'https://6fra5t373m.execute-api.eu-west-1.amazonaws.com/development/sessions/' + token;
        return fetch(url,{
            method: "DELETE",
            headers: {
                Authorization: token,
            },
        }).then(localStorage.removeItem("sessionToken"));
    }

    render(){
        return(
            <div className="super-cabecera">
                <nav>
                    <ul>
                        <li>
                            <span>{this.state.name}</span>
                        </li>
                        <li>
                        <Link to="/">
                            <button className="main-content" onClick={(e) => this.handleSubmit(e)} type="submit">Cerrar Sesión</button>
                        </Link>
                        </li>
                    </ul>
                </nav>
            </div>
        );
    }
}
export default LogOut;