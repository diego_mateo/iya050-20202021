import React, {Component} from 'react';
import { Link, Redirect } from "react-router-dom";
import './Form.css';

class Registro extends Component{

    constructor(props){
        super(props);
        this.state = {
            username: "",
            firstname: "",
            password: "",
            isReg: false,
            error: false,
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(e) {
        const {name, value} = e.target;
        this.setState({[name]: value});
    }

    handleSubmit(e){
        e.preventDefault();

        //this.setState({isReg: true});
        const user = this.state.username;
        const name = this.state.firstname;
        const pass = this.state.password;
        const isReg = this.state.isReg;
        // stop here if form is invalid
        if (!(name && user && pass)) {
            return;
        }
        
        const url = "https://6fra5t373m.execute-api.eu-west-1.amazonaws.com/development/users/" + user;
        return fetch(url, {
            method : "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({ firstName: name, password: pass}),
        })
        .then(response => response.json())
        .then((data) => {
            if (data.hasOwnProperty("firstName")) this.setState({ isReg: true });
            else {
                this.setState({ error: true });
            }
            console.log(this.state.isReg);
            console.log(user);
            console.log(name);
            console.log(pass);
            console.log(isReg);
        });

    }

    render(){

        const username = this.state.username;
        const password = this.state.password;
        const name = this.state.firstname;
        const isReg = this.state.isReg;
        //const error = this.state.error;     

        var redirect = "";
        if (isReg) {
        redirect = <Redirect to="/" />;
        }

        return (
            <div className="contact-wrapper top-wrapper">
                <form className="form" onSubmit={this.handleSubmit}>
                    <h3>Registro</h3>

                    <div className="input">
                        <label>Nombre completo</label>
                        <input type="text" name="firstname" value={name} onChange={this.handleChange} />
                        {isReg && !name && <div className="help-block">Nombre requerido</div>}
                    </div>

                    <div className="input">
                        <label>Nombre de usuario</label>
                        <input type="text" name="username" value={username} onChange={this.handleChange} />
                        {isReg && !username && <div className="help-block">Nombre de usuario requerido</div>}
                    </div>

                    <div className="input">
                        <label>Contraseña</label>
                        <input type="text" name="password" value={password} onChange={this.handleChange} />
                        {isReg && !password && <div className="help-block">Contraseña requerida</div>}
                    </div>

                    <button type="submit" className="btn btn-primary btn-block">Registrarse</button>
                    <p className="forgot-password text-right">
                        ¿Ya estas registrado? <Link to="/">¿Quieres acceder?</Link>
                    </p>
                </form>
                {redirect}
            </div>
        );
    }

}

export default Registro;