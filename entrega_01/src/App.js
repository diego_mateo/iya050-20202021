import React from 'react';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Registro from './components/Registro.js';
import LogIn from './components/LogIn.js';
import dado from './components/Dado';

function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/" component={LogIn} />
        <Route path="/registro" component={Registro} />
        <Route path="/game" component={dado} />
      </Switch>
    </Router>
  );
}

export default App;